from flask import Flask, request
from block_chain import Blockchain, Block
import json

app = Flask(__name__)

# Contains the host addresses of other participating members of the network
PEERS = []

# The server version of the block chain
block_chain = Blockchain()


# POST for registering a new node
@app.route('/register_node', methods=['POST'])
def register_node():
    # Getting the request object and checking conformity
    the_request = request.get_json()
    if not node_address_key_is_in_request(the_request):
        return "Invalid data", 400
    # If the request object is conform, adding the new address to the list of PEERS
    else:
        node_address = the_request["node_address"]
        PEERS.append(node_address)
        return "Success", 200


# POST for adding a new model for registered users
@app.route('/add_model', methods=['POST'])
def add_model():
    # Getting the request object and checking conformity
    the_request = request.get_json()
    if not node_address_key_is_in_request(the_request):
        return "Invalid data", 400
    elif not node_address_key_is_registered(the_request):
        return "User is not allowed", 400
    # If the request object is conform, adding the model to the list of models
    else:
        block_chain.models.append(the_request["model_to_add"])
        return "Model was successfully added. use /mine for adding it to the block chain.", 200


# POST for adding a new model for registered users
@app.route('/mine', methods=['POST'])
def mine():
    # Getting the request object and checking conformity
    the_request = request.get_json()
    if not node_address_key_is_in_request(the_request):
        return "Invalid data", 400
    elif not node_address_key_is_registered(the_request):
        return "User is not allowed", 400
    # If the request object is conform, we check if we have models to mine
    elif block_chain.models:
        block_chain.mine()
        return "Success", 200
    else:
        return "No models to mine", 200


# POST for getting the current version of the chain
@app.route('/get_chain', methods=['POST'])
def get_chain():
    # Getting the request object and checking conformity
    the_request = request.get_json()
    if not node_address_key_is_in_request(the_request):
        return "Invalid data", 400
    elif not node_address_key_is_registered(the_request):
        return "User is not allowed", 400
    # If the request object is conform, we return the block chain
    else:
        chain_data = []
        for a_block in block_chain.chain:
            chain_data.append(a_block.__dict__)
        return json.dumps(chain_data), 200


def node_address_key_is_in_request(the_request):
    return "node_address" in the_request.keys()


def node_address_key_is_registered(the_request):
    node_address = the_request["node_address"]
    return node_address in PEERS


def create_chain_from_json_dumps(json_dumps):
    """
    Helper function to create a Block Chain from a json sent by a client
    :param json_dumps: (list) containing dictionaries (equivalents of jsons in python) representing blocks
    :return: (Blockchain) the associated block chain
    """
    returned_chain = Blockchain()
    for idx_block, a_block in enumerate(json_dumps):
        a_block = Block(
            index=a_block["index"],
            data=a_block["data"],
            previous_hash=a_block["previous_hash"],
            timestamp=a_block["timestamp"],
            nonce=a_block["nonce"],
            hash=a_block["hash"]
        )
        if idx_block == 0:  # If this is the genesis block we replace it in the chain
            returned_chain.chain[idx_block] = a_block
        else:  # If this is not the genesis block, we add it to the chain
            returned_chain.add_block(a_block)
    return returned_chain
