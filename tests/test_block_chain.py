import unittest
from block_chain import Block, Blockchain


class TestBlockChain(unittest.TestCase):

    def setUp(self) -> None:
        self.block_chain = Blockchain()

    def test_block_chain_has_only_one_genesis_block_when_is_instanciated(self):
        # GIVEN
        expected_length = 1
        # WHEN
        chain_length = len(self.block_chain.chain)
        # THEN
        self.assertEqual(expected_length, chain_length)

    def test_genesis_block_is_a_block_object(self):
        # GIVEN
        # WHEN
        block = self.block_chain.chain[0]
        # THEN
        self.assertIsInstance(block, Block)

    def test_proof_of_work_generates_a_hash_that_starts_with_the_right_number_of_zeros(self):
        # GIVEN
        block = Block()
        self.block_chain.mining_difficulty_level = 2
        sequence_of_zeros = "00"
        # WHEN
        hash_generated_by_proof_of_work = self.block_chain.proof_of_work(block)[0]
        # THEN
        self.assertTrue(hash_generated_by_proof_of_work.startswith(sequence_of_zeros))

    def test_add_block_does_not_add_block_if_previous_hash_is_not_correct(self):
        # GIVEN
        a_block = Block(previous_hash="garbage_hash")
        a_block.hash, a_block.nonce = Blockchain.proof_of_work(a_block)
        # WHEN
        self.block_chain.add_block(a_block)
        # THEN
        self.assertNotIn(a_block, self.block_chain.chain)

    def test_add_block_does_not_add_block_if_hash_is_not_valid(self):
        # GIVEN
        hash_of_the_last_block = self.block_chain.get_last_block.hash
        a_block = Block(previous_hash=hash_of_the_last_block)
        # WHEN
        self.block_chain.add_block(a_block)
        # THEN
        self.assertNotIn(a_block, self.block_chain.chain)

    def test_add_block_does_not_add_block_if_index_is_not_valid(self):
        # GIVEN
        hash_of_the_last_block = self.block_chain.get_last_block.hash
        a_block = Block(previous_hash=hash_of_the_last_block)
        a_block.hash, a_block.nonce = Blockchain.proof_of_work(a_block)
        # WHEN
        self.block_chain.add_block(a_block)
        # THEN
        self.assertNotIn(a_block, self.block_chain.chain)

    def test_add_block_does_add_a_valid_block(self):
        # GIVEN
        hash_of_the_last_block = self.block_chain.get_last_block.hash
        index_of_the_last_block = self.block_chain.get_last_block.index
        a_block = Block(previous_hash=hash_of_the_last_block, index=index_of_the_last_block + 1)
        a_block.hash, a_block.nonce = Blockchain.proof_of_work(a_block)
        # WHEN
        self.block_chain.add_block(a_block)
        # THEN
        self.assertIn(a_block, self.block_chain.chain)

    def test_add_block_does_not_modify_nonce_if_block_is_already_valid(self):
        # GIVEN
        hash_of_the_last_block = self.block_chain.get_last_block.hash
        index_of_the_last_block = self.block_chain.get_last_block.index
        a_block = Block(previous_hash=hash_of_the_last_block, index=index_of_the_last_block + 1)
        a_block_hash, a_block_nonce = Blockchain.proof_of_work(a_block)
        a_block.hash, a_block.nonce = a_block_hash, a_block_nonce
        # WHEN
        self.block_chain.add_block(a_block)
        # THEN
        self.assertIn(self.block_chain.get_last_block.hash, a_block_hash)


if __name__ == '__main__':
    unittest.main()
