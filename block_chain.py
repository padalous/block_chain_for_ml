import json
from hashlib import sha256
import time


class Block:
    def __init__(self, index=0, data="", previous_hash=0, nonce=0, timestamp=time.time(), hash="1"*64):
        """
        Constructor for the `Block` class.
        :param index: Unique ID of the block, useful inside a block chain.
        :param data: Data to be stored in the block.
        :param previous_hash: Hash of the previous block in the chain which this block is part of.
        :param nonce: Number only used once, the index used to calculate a valid proof of work in a block chain.
        :param timestamp: Time when our block is created
        """
        self.index = index
        self.data = data
        self.previous_hash = previous_hash
        self.nonce = nonce
        self.hash = hash
        self.timestamp = timestamp

    def compute_hash(self):
        """
        Returns the hash of the block instance by first converting it into JSON string.
        """
        block_string = json.dumps(self.__dict__, sort_keys=True)
        return sha256(block_string.encode()).hexdigest()

    def __repr__(self):
        return str(self.__dict__)


class Blockchain:
    mining_difficulty_level = 2  # The difficulty level for mining blocks. Refer to proof_of_work method

    def __init__(self):
        """
        Constructor of the BlockChain class. Initialises a chain with a genesis block inside it.
        """
        self.chain = []
        self.models = []
        self.add_genesis_block_to_chain()

    def add_genesis_block_to_chain(self):
        """
        Generate genesis block and add it to the chain. The block has index 0,
        previous_hash as 0, and a valid hash.
        """
        genesis_block = Block()
        genesis_block.hash, genesis_block.nonce = Blockchain.proof_of_work(genesis_block)
        self.chain.append(genesis_block)

    @property
    def get_last_block(self):
        return self.chain[-1]

    @staticmethod
    def proof_of_work(a_block):
        """
        Tries different values of the nonce to get a hash that satisfies our difficulty criteria.
        """
        computed_hash = a_block.compute_hash()
        while not Blockchain.is_hash_valid(computed_hash):
            a_block.nonce += 1
            computed_hash = a_block.compute_hash()

        return computed_hash, a_block.nonce

    @staticmethod
    def is_hash_valid(a_hash):
        """
        Check if a hash respects the validation rule : i.e. does starts with n zeros, n being the
        mining_difficulty_level of the BlockChain object.
        :param a_hash: (str) a hash-like string.
        :return: (bool) A boolean True if the hash is valid.
        """
        return a_hash.startswith("0" * Blockchain.mining_difficulty_level)

    def add_block(self, block):
        """
        Adds the block to the chain after conformity verification. Verification includes:
        * Checking if the hash is valid.
        * The previous_hash referred in the block and the hash of a latest block in the chain match.
        * The index of the block follows the index of the last block in the chain
        """
        # Getting usefull data for checking block conformity
        previous_hash = self.get_last_block.hash
        previous_index = self.get_last_block.index
        # Checking block conformity
        if previous_hash != block.previous_hash:
            return False
        elif not Blockchain.is_hash_valid(block.hash):
            return False
        elif previous_index + 1 != block.index:
            return False
        # If the block is valid add it to the chain
        else:
            self.chain.append(block)
            return True

    def mine(self):
        """
        Create one block for each model, mine it, and add it to the chain.
        """
        for a_model in self.models:
            # Get info about the last block of the chain (index, previous hash)
            index_for_new_block, previous_hash = self.get_last_block.index + 1, self.get_last_block.hash
            # Create a new block with all this info
            block_to_add = Block(index=index_for_new_block,
                                 data=a_model,
                                 previous_hash=previous_hash)
            # Use proof_of_work to mine the block
            block_to_add.hash, block_to_add.nonce = Blockchain.proof_of_work(block_to_add)
            # Add the block to the chain
            self.add_block(block_to_add)

    def __repr__(self):
        return str([block for block in self.chain])
